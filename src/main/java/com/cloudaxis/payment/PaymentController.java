package com.cloudaxis.payment;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class PaymentController {	
	/*
	 * card payments
	 */
	@RequestMapping(value="/cardPaymentsResult", method=RequestMethod.POST)
	public void getSomethingThird(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String apiUrl = "https://pal-test.adyen.com/pal/servlet/Payment/v18/authorise";
		String wsUser = "ws@Company.Cloudaxis";
		String wsPassword = "dsmdduzxgf7x";
		
		CredentialsProvider provider = new BasicCredentialsProvider();
		UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(wsUser, wsPassword);
		provider.setCredentials(AuthScope.ANY, credentials);
		
		HttpClient client = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();
			
		// Create new payment request
		JSONObject paymentRequest = new JSONObject();
		paymentRequest.put("merchantAccount", "CloudaxisCOM");
		paymentRequest.put("reference", "this is a reference " + new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss").format(new Date()));

		// Set amount
		JSONObject amount = new JSONObject();
		amount.put("currency", "EUR");
		amount.put("value", 2530);
		paymentRequest.put("amount", amount);
		
		// Set additionalData
		JSONObject additionalData = new JSONObject();
		additionalData.put("card.encrypted.json", request.getParameter("adyen-encrypted-data"));
		paymentRequest.put("additionalData", additionalData);		
		System.out.println(paymentRequest);
		
		HttpPost httpRequest = new HttpPost(apiUrl);
		httpRequest.addHeader("Content-Type", "application/json");
		httpRequest.setEntity(new StringEntity(paymentRequest.toString(), "UTF-8"));

		HttpResponse httpResponse = client.execute(httpRequest);
		String paymentResponse = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
		
		// Parse JSON response
		JSONParser parser = new JSONParser();
		JSONObject paymentResult;
		
		try {
			paymentResult = (JSONObject) parser.parse(paymentResponse);
		} catch (ParseException e) {
			throw new ServletException(e);
		}
		

		if (httpResponse.getStatusLine().getStatusCode() != 200) {
			String faultString = paymentResult.get("errorType") + " " + paymentResult.get("errorCode") + " " + paymentResult.get("message");
			throw new ServletException(faultString);
		}

		PrintWriter out = response.getWriter();

		out.println("Payment Result:");
		out.println("- pspReference: " + paymentResult.get("pspReference"));
		out.println("- resultCode: " + paymentResult.get("resultCode"));
		out.println("- authCode: " + paymentResult.get("authCode"));
		out.println("- refusalReason: " + paymentResult.get("refusalReason"));
		
		
		Properties prop = new Properties(); 
		File file = new File("order.properties");
		if (file.exists()) {
			file.delete();
		}

		OutputStream oFile = new FileOutputStream("order.properties", false);
		prop.setProperty("pspReference", paymentResult.get("pspReference").toString());
		prop.setProperty("resultCode", paymentResult.get("resultCode").toString());
		prop.setProperty("authCode", paymentResult.get("authCode").toString());
		prop.setProperty("refusalReason", paymentResult.get("refusalReason")==null?"":paymentResult.get("refusalReason").toString());
		prop.store(oFile, "The New properties file");
		oFile.flush();
		oFile.close();
	}
	
	
	/*
	 * recurring payments
	 */
	@RequestMapping(value="/recurringPaymentsResult", method=RequestMethod.POST)
	public void getRecurringPaymentsResult(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String apiUrl = "https://pal-test.adyen.com/pal/servlet/Payment/v18/authorise";
		String wsUser = "ws@Company.Cloudaxis";
		String wsPassword = "dsmdduzxgf7x";
		
		CredentialsProvider provider = new BasicCredentialsProvider();
		UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(wsUser, wsPassword);
		provider.setCredentials(AuthScope.ANY, credentials);
		
		HttpClient client = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();		
		
		// Create new payment request
		JSONObject paymentRequest = new JSONObject();
		paymentRequest.put("merchantAccount", "CloudaxisCOM");
		paymentRequest.put("reference", "this is a reference " + new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss").format(new Date()));
		paymentRequest.put("shopperEmail", "highlight.li@cloudaxis.com");
		paymentRequest.put("shopperReference", "shopper reference uniquely identifies the shopper");
		
		JSONObject recurring = new JSONObject();
		recurring.put("contract", "RECURRING");
		paymentRequest.put("recurring", recurring);
		
		// Set amount
		JSONObject amount = new JSONObject();
		amount.put("currency", "EUR");
		amount.put("value", 2530);
		paymentRequest.put("amount", amount);
		
		// Set additionalData
		JSONObject additionalData = new JSONObject();
		additionalData.put("card.encrypted.json", request.getParameter("adyen-encrypted-data"));
		paymentRequest.put("additionalData", additionalData);		
		System.out.println(paymentRequest);
		
		HttpPost httpRequest = new HttpPost(apiUrl);
		httpRequest.addHeader("Content-Type", "application/json");
		httpRequest.setEntity(new StringEntity(paymentRequest.toString(), "UTF-8"));

		HttpResponse httpResponse = client.execute(httpRequest);
		String paymentResponse = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");		
		
		// Parse JSON response
		JSONParser parser = new JSONParser();
		JSONObject paymentResult;
		
		try {
			paymentResult = (JSONObject) parser.parse(paymentResponse);
		} catch (ParseException e) {
			throw new ServletException(e);
		}
		

		if (httpResponse.getStatusLine().getStatusCode() != 200) {
			String faultString = paymentResult.get("errorType") + " " + paymentResult.get("errorCode") + " " + paymentResult.get("message");
			throw new ServletException(faultString);
		}

		PrintWriter out = response.getWriter();

		out.println("Payment Result:");
		out.println("- pspReference: " + paymentResult.get("pspReference"));
		out.println("- resultCode: " + paymentResult.get("resultCode"));
		out.println("- authCode: " + paymentResult.get("authCode"));
		out.println("- refusalReason: " + paymentResult.get("refusalReason"));
	}
	
	/*
	 * card payments homepage
	 */
	@RequestMapping(value={"/","/cardPayments"}, method=RequestMethod.GET)
	public String home2(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Generate current time server-side and set it as request attribute
		request.setAttribute("generationTime", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX").format(new Date()));

		// Forward request to corresponding JSP page
		return "create-card-payment-cse";
	}
	
	/*
	 * recurring payments homepage
	 */
	@RequestMapping(value="/recurringPayments/creation", method=RequestMethod.GET)
	public String home3(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Generate current time server-side and set it as request attribute
		request.setAttribute("generationTime", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX").format(new Date()));

		// Forward request to corresponding JSP page
		return "create-recurring-payment-cse";
	}
	
	/*
	 * recurring payments submission
	 */
	@RequestMapping(value="/recurringPayments/submission", method=RequestMethod.GET)
	public void home4(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String apiUrl = "https://pal-test.adyen.com/pal/servlet/Payment/v18/authorise";
		String wsUser = "ws@Company.Cloudaxis";
		String wsPassword = "dsmdduzxgf7x";
		
		CredentialsProvider provider = new BasicCredentialsProvider();
		UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(wsUser, wsPassword);
		provider.setCredentials(AuthScope.ANY, credentials);
		
		HttpClient client = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();		
		
		// Create new payment request
		JSONObject paymentRequest = new JSONObject();
		paymentRequest.put("merchantAccount", "CloudaxisCOM");
		paymentRequest.put("reference", "this is a reference " + new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss").format(new Date()));
		paymentRequest.put("shopperEmail", "highlight.li@cloudaxis.com");
		paymentRequest.put("shopperReference", "shopper reference uniquely identifies the shopper");
		paymentRequest.put("selectedRecurringDetailReference", "LATEST");
		paymentRequest.put("shopperInteraction", "ContAuth");
		
		JSONObject recurring = new JSONObject();
		recurring.put("contract", "RECURRING");
		paymentRequest.put("recurring", recurring);
		
		// Set amount
		JSONObject amount = new JSONObject();
		amount.put("currency", "EUR");
		amount.put("value", 2540);
		paymentRequest.put("amount", amount);
		
		HttpPost httpRequest = new HttpPost(apiUrl);
		httpRequest.addHeader("Content-Type", "application/json");
		httpRequest.setEntity(new StringEntity(paymentRequest.toString(), "UTF-8"));

		HttpResponse httpResponse = client.execute(httpRequest);
		String paymentResponse = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");		
		
		// Parse JSON response
		JSONParser parser = new JSONParser();
		JSONObject paymentResult;
		
		try {
			paymentResult = (JSONObject) parser.parse(paymentResponse);
		} catch (ParseException e) {
			throw new ServletException(e);
		}
		

		if (httpResponse.getStatusLine().getStatusCode() != 200) {
			String faultString = paymentResult.get("errorType") + " " + paymentResult.get("errorCode") + " " + paymentResult.get("message");
			throw new ServletException(faultString);
		}

		PrintWriter out = response.getWriter();

		out.println("Payment Result:");
		out.println("- pspReference: " + paymentResult.get("pspReference"));
		out.println("- resultCode: " + paymentResult.get("resultCode"));
		out.println("- authCode: " + paymentResult.get("authCode"));
		out.println("- refusalReason: " + paymentResult.get("refusalReason"));	
	}
	
	@RequestMapping(value="/payments/capture", method=RequestMethod.GET)
	public void getSomethingFourth(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String apiUrl = "https://pal-test.adyen.com/pal/servlet/Payment/v18/capture";
		String wsUser = "ws@Company.Cloudaxis";
		String wsPassword = "dsmdduzxgf7x";
		
		CredentialsProvider provider = new BasicCredentialsProvider();
		UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(wsUser, wsPassword);
		provider.setCredentials(AuthScope.ANY, credentials);
		
		HttpClient client = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();
		
		Properties prop = new Properties(); 		
		InputStream in = new BufferedInputStream(new FileInputStream("order.properties"));
		prop.load(in);
			
		// Create new payment request
		JSONObject paymentRequest = new JSONObject();
		paymentRequest.put("merchantAccount", "CloudaxisCOM");
		paymentRequest.put("reference", "capture reference " + new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss").format(new Date()));
		paymentRequest.put("originalReference", prop.getProperty("pspReference"));
		
		JSONObject modificationAmount = new JSONObject();
		modificationAmount.put("value", 30100);
		modificationAmount.put("currency", "EUR");
		paymentRequest.put("modificationAmount", modificationAmount);
		
		HttpPost httpRequest = new HttpPost(apiUrl);
		httpRequest.addHeader("Content-Type", "application/json");
		httpRequest.setEntity(new StringEntity(paymentRequest.toString(), "UTF-8"));

		HttpResponse httpResponse = client.execute(httpRequest);
		String paymentResponse = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
		
		// Parse JSON response
		JSONParser parser = new JSONParser();
		JSONObject paymentResult;
		
		try {
			paymentResult = (JSONObject) parser.parse(paymentResponse);
		} catch (ParseException e) {
			throw new ServletException(e);
		}
		

		if (httpResponse.getStatusLine().getStatusCode() != 200) {
			String faultString = paymentResult.get("errorType") + " " + paymentResult.get("errorCode") + " " + paymentResult.get("message");
			throw new ServletException(faultString);
		}

		PrintWriter out = response.getWriter();

		out.println("Payment Result:");
		out.println("- pspReference: " + paymentResult.get("pspReference"));
		out.println("- resultCode: " + paymentResult.get("response"));
		
		in.close();
	}
	
	@RequestMapping(value="/payments/cancel", method=RequestMethod.GET)
	public void getSomethingFifth(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String apiUrl = "https://pal-test.adyen.com/pal/servlet/Payment/v18/cancel";
		String wsUser = "ws@Company.Cloudaxis";
		String wsPassword = "dsmdduzxgf7x";
		
		CredentialsProvider provider = new BasicCredentialsProvider();
		UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(wsUser, wsPassword);
		provider.setCredentials(AuthScope.ANY, credentials);
		
		HttpClient client = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();
		
		Properties prop = new Properties(); 		
		InputStream in = new BufferedInputStream(new FileInputStream("order.properties"));
		prop.load(in);
			
		// Create new payment request
		JSONObject paymentRequest = new JSONObject();
		paymentRequest.put("merchantAccount", "CloudaxisCOM");
		paymentRequest.put("originalReference", prop.getProperty("pspReference"));
		
		HttpPost httpRequest = new HttpPost(apiUrl);
		httpRequest.addHeader("Content-Type", "application/json");
		httpRequest.setEntity(new StringEntity(paymentRequest.toString(), "UTF-8"));

		HttpResponse httpResponse = client.execute(httpRequest);
		String paymentResponse = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
		
		// Parse JSON response
		JSONParser parser = new JSONParser();
		JSONObject paymentResult;
		
		try {
			paymentResult = (JSONObject) parser.parse(paymentResponse);
		} catch (ParseException e) {
			throw new ServletException(e);
		}
		

		if (httpResponse.getStatusLine().getStatusCode() != 200) {
			String faultString = paymentResult.get("errorType") + " " + paymentResult.get("errorCode") + " " + paymentResult.get("message");
			throw new ServletException(faultString);
		}

		PrintWriter out = response.getWriter();

		out.println("Payment Result:");
		out.println("- pspReference: " + paymentResult.get("pspReference"));
		out.println("- resultCode: " + paymentResult.get("response"));
		
		in.close();
	}
}
