<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Adyen - Client-Side Encryption</title>
	</head>
	<body>
		<form method="POST" action="cardPaymentsResult" target="_blank" id="adyen-encrypted-form">
			<fieldset>
				<legend>Card Details</legend>
				
				<div class="field">
					<label for="adyen-encrypted-form-number">Card Number
						<input type="text" id="adyen-encrypted-form-number" value="5555444433331111" size="20" autocomplete="off" data-encrypted-name="number">
					</label>
				</div>
				
				<div class="field">
					<label for="adyen-encrypted-form-holder-name">Card Holder Name
						<input type="text" id="adyen-encrypted-form-holder-name" value="John Doe" size="20" autocomplete="off" data-encrypted-name="holderName">
					</label>
				</div>
				
				<div class="field">
					<label for="adyen-encrypted-form-cvc">CVC
						<input type="text" id="adyen-encrypted-form-cvc" value="737" size="4" autocomplete="off" data-encrypted-name="cvc">
					</label>
				</div>
				
				<div class="field">
					<label for="adyen-encrypted-form-expiry-month">Expiration Month (MM)
						<input type="text" value="06" id="adyen-encrypted-form-expiry-month" size="2" autocomplete="off" data-encrypted-name="expiryMonth"> /
					</label>
					<label for="adyen-encrypted-form-expiry-year">Expiration Year (YYYY)
						<input type="text" value="2016" id="adyen-encrypted-form-expiry-year" size="4" autocomplete="off" data-encrypted-name="expiryYear">
					</label>
				</div>
				
				<div class="field">
					<input type="hidden" id="adyen-encrypted-form-expiry-generationtime" value="${generationTime}" data-encrypted-name="generationtime">
					<input type="submit" value="Create payment">
				</div>
			</fieldset>
		</form>
	
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/adyen.encrypt.min.js"></script>
		<script type="text/javascript">
			var form = document.getElementById('adyen-encrypted-form');
			
			// Put your WS users' CSE key here
			// Adyen CA -> Settings -> Users -> Choose the WS user -> Copy CSE key
			var key = "10001|BA095B85E6AF7E1CB4E6102E549C2FC205A83B9795E584EF156AFC2EEB1BE5CF7C20D684E0F66413C7B0868CE4A3BCD5EF970687015D06AC3E67D31BCB37E1A4343532FA4C8C44E79134B068805934821F9409A917F2C0E96499854F1D11785251C7FB80B8C7EC1AF01A39CB3A9B3DA82A22B3D8E2CFDDDA68DD2E4E68A4B3749683F7254F487F0FFB9565C77AD5DC92D98D8C2543CB6CDD15B8F0238F10873BB83E0F2C368C49135B118141BB0BCEC207134C9767354A0817D0EEA59F1287877BF907728896F9D690C422C87DAA224BAC8F55804921701A0F6FC7D7494F7E9A75DC617ACE6F5CE195C557A055AE2104A15A829D6FFBE8BA12434FA7DDD24423";
			
			adyen.encrypt.createEncryptedForm(form, key, {});
		</script>
	</body>
</html>