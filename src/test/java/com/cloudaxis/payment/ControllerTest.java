package com.cloudaxis.payment;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;

public class ControllerTest {
	
	  @Test
	  public void testHomePage() throws Exception {
	    PaymentController controller = new PaymentController();
	    MockMvc mockMvc = standaloneSetup(controller).build();
	    mockMvc.perform(get("/"))
	           .andExpect(view().name("create-card-payment-cse"));
	  }
}
